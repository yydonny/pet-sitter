
param (
    [string]$command,
    [string]$environment = 'development'
)

$flywayCommand = 'Info'
if ($command -eq 'migrate')
{
    $flywayCommand = 'Migrate'
}
elseif ($command -eq 'clean')
{
    $flywayCommand = 'Clean'
}

echo ''
echo "----- Running flyway$flywayCommand for the envrionment '$environment' -----"

gradle "-PbuildEnv=$environment" `
  "-Dflyway.configFiles=environments/$environment.properties" `
  "flyway$flywayCommand"
