
-- store authorities along with the user to avoid additional query
alter table auth.users add column roles text check (roles <> '');
