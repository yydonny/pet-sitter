
-- createdb -U <admin> -O yangyd petsitter

-- database schemas for Spring Security JDBC UserDetailsService implementation.

-- Auth tables are designed without ORM im mind, thus should not be accessed via Hibernate.

drop schema if exists auth cascade;
create schema auth;

create table auth.users (
	id bigserial not null primary key,
	username varchar(50) not null unique,
	password varchar(100) not null,
	enabled boolean not null);

create table auth.authorities (
    user_id bigint not null,
	authority varchar(50) not null,
	constraint fk_authorities_users foreign key(user_id) references auth.users(id)
);

create unique index ix_auth_username on auth.authorities (user_id,authority);

create table auth.groups (
	id bigserial not null primary key,
	group_name varchar(50) not null
);

create table auth.group_authorities (
	group_id bigint not null,
	authority varchar(50) not null,
	constraint fk_group_authorities_group foreign key(group_id) references auth.groups(id)
);

create table auth.group_members (
	id bigserial not null primary key,
	user_id bigint not null,
	group_id bigint not null,
	constraint fk_group_members_group foreign key(group_id) references auth.groups(id)
);
