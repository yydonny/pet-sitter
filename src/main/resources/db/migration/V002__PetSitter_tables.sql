

--Hibernate:
-- public. is necessary here, otherwise it goes to the default schema configured for Flyway
drop sequence if exists public.hibernate_sequence cascade;
create sequence public.hibernate_sequence start 1 increment 1;

drop schema if exists petsitter cascade;
create schema petsitter;

--Hibernate:
create table petsitter.user_detail (
    id int8 not null,
    enabled boolean not null,
    first_name varchar(255),
    last_name varchar(255),
    password varchar(255) not null,
    userType int4 not null,
    username varchar(255) not null,
    version int4 not null,
    primary key (id));

--Hibernate:
create table petsitter.Pet (
    id int8 not null,
    created_at timestamp not null,
    version int4 not null,
    age int4,
    name varchar(255) not null,
    petType int4,
    rfid varchar(255),
    owner_id int8 not null,
    primary key (id));

--Hibernate:
alter table petsitter.Pet add constraint UK_jqswrifxqudfiwgnovd7r72u4 unique (name);

--Hibernate:
alter table petsitter.user_detail add constraint UK_bw2k2v2ql8t36dw09nel1h62s unique (username);

--Hibernate:
alter table petsitter.Pet
    add constraint FK5xrmau1hm9ttu8hq0qcnhiya7
    foreign key (owner_id) references petsitter.user_detail
