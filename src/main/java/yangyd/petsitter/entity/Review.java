package yangyd.petsitter.entity;

import yangyd.petsitter.entity.enums.ReviewGrade;

public class Review {
  private PetSitterRequest request;
  private PetSitterResponse response;
  private ReviewGrade grade;
  private String detail;


  public PetSitterRequest getRequest() {
    return request;
  }

  public void setRequest(PetSitterRequest request) {
    this.request = request;
  }

  public PetSitterResponse getResponse() {
    return response;
  }

  public void setResponse(PetSitterResponse response) {
    this.response = response;
  }

  public ReviewGrade getGrade() {
    return grade;
  }

  public void setGrade(ReviewGrade grade) {
    this.grade = grade;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }
}
