package yangyd.petsitter.entity;

import yangyd.petsitter.entity.enums.RequestStatus;

import java.util.Date;
import java.util.Set;

/**
 * A user account of userType OWNER is able to create a request for a pet sitter for more than one interval.
 */
public class PetSitterRequest {

  private User user;
  private Date startAt;
  private Date endAt;

  private String detail;

  private RequestStatus status;

  private Set<Pet> pets;
  private Set<PetSitterResponse> responses;

  public Date getEndAt() {
    return endAt;
  }

  public void setEndAt(Date endAt) {
    this.endAt = endAt;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Date getStartAt() {
    return startAt;
  }

  public void setStartAt(Date startAt) {
    this.startAt = startAt;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public RequestStatus getStatus() {
    return status;
  }

  public void setStatus(RequestStatus status) {
    this.status = status;
  }

  public Set<Pet> getPets() {
    return pets;
  }

  public void setPets(Set<Pet> pets) {
    this.pets = pets;
  }

  public Set<PetSitterResponse> getResponses() {
    return responses;
  }

  public void setResponses(Set<PetSitterResponse> responses) {
    this.responses = responses;
  }
}
