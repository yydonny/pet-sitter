package yangyd.petsitter.entity.enums;

public enum PetType {
  // Stored as ORDINAL value in database.
  // never delete anything or change the ordering!

  cat, // 1
  dog, // 2

  // add other type
}
