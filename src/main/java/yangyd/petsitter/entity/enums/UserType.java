package yangyd.petsitter.entity.enums;

public enum UserType {
  // Stored as ORDINAL value in database.
  // never delete anything or change the ordering!

  disabled, //0
  owner, // 1
  sitter, // 2
  both, // 3
  admin; // 4

  public static UserType lookup(int ord) {
    return UserType.values()[ord];
  }
  // add more
}
