package yangyd.petsitter.entity.enums;

public enum  ReviewGrade {
  poor, normal, good
}
