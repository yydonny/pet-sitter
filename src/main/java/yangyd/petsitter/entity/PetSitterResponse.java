package yangyd.petsitter.entity;

import yangyd.petsitter.entity.enums.ResponseStatus;

/**
 * A user account of userType SITTER can reply to requests by creating response objects
 * that will be approved or rejected by the owner of that request.
 */
public class PetSitterResponse {
  private User user;
  private PetSitterRequest request;
  private ResponseStatus status;
  private String detail;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public PetSitterRequest getRequest() {
    return request;
  }

  public void setRequest(PetSitterRequest request) {
    this.request = request;
  }

  public ResponseStatus getStatus() {
    return status;
  }

  public void setStatus(ResponseStatus status) {
    this.status = status;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }
}
