package yangyd.petsitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import yangyd.petsitter.configuration.JPAConfig;

import java.util.stream.Stream;

/**
 * Commandline Launcher of the application for testing
 */
public class Main {
  private static final Logger logger = LoggerFactory.getLogger(Main.class);
  public static void main(String[] args) {
    // if started from sevelet container (e.g. Tomcat), set the property like this
    // $env:JAVA_OPTS="-Dhikari.properties.file=/hikari.properties"
    // or better write a ServletContextListener that pick up parameters from web.xml
    if (System.getProperty(JPAConfig.HIKARI_PROP_FILE) == null) {
      logger.warn("Database/Connection pool configuration not found. Fallback to default hikari.properties");
      System.setProperty(JPAConfig.HIKARI_PROP_FILE, "/hikari.properties");
    }

    System.setProperty("spring.profiles.active", "development");
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:/application.xml");

    if (logger.isDebugEnabled()) {
      Stream.of(context.getBeanDefinitionNames()).sorted().forEach(n ->
          logger.debug("Created bean: {} ({})", n, context.getBean(n).getClass().getSimpleName()));
    }
    context.close();
  }
}
