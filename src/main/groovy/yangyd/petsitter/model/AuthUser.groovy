package yangyd.petsitter.model

class AuthUser {
  long id
  String username
  String hashedPassword
  boolean enabled

  /**
   * Comma separated role names
   */
  String[] roles
}
