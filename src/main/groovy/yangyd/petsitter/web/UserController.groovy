package yangyd.petsitter.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import yangyd.petsitter.service.UserService

@Controller
@RequestMapping("/user")
class UserController {
  private static final Logger logger = LoggerFactory.getLogger(UserController)
  private final UserService service

  UserController(UserService userService) {
    this.service = userService
  }

  @RequestMapping
  String index(Model model) {
    logger.error("============== you see me! ==================")
    model.addAttribute("users", service.findAll())
    "user/index"
  }

  @RequestMapping("/{id:\\d*}")
  String detail(Long id, Model model) {
    model.addAttribute("user", service.findById(id))
    "user/detail"
  }


}
