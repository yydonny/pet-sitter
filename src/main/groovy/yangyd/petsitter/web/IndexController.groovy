package yangyd.petsitter.web

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/")
class IndexController {
  private static final logger = LoggerFactory.getLogger(IndexController)

  @RequestMapping
  String index() {
    "index"
  }

  /**
   * GET /login
   * Displays the login form. an error flag is set if an error request parameter is
   * present (which is the case when redirected from Spring security's login handler).
   * The error flag handling should ideally be done client-side with javascript.
   */
  @RequestMapping("/login")
  String auth(Model model, HttpServletRequest request) {
    Enumeration<String> names = request.parameterNames
    while (names.hasMoreElements()) {
      String s = names.nextElement()
      if (s.equals("error")) {
        logger.debug("-- displaying login form with error indicator")
        model.addAttribute("error", "1")
        break
      }
    }
    "login-form"
  }
}
