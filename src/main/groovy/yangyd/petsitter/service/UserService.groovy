package yangyd.petsitter.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import yangyd.petsitter.entity.User
import yangyd.petsitter.repository.UserRepository

@Service
class UserService extends AbstractCrudService<User> {
  final UserRepository repository

  @Autowired
  UserService(UserRepository repository) {
    this.repository = repository
  }
}
