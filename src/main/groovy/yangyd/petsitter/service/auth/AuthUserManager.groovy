package yangyd.petsitter.service.auth

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DuplicateKeyException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import yangyd.petsitter.model.AuthUser
import yangyd.petsitter.model.Role
import yangyd.petsitter.repository.jdbc.AuthUserDAO

import java.util.stream.Collectors
/**
 * A simplified implementation of {@code UserDetailsService}, used by {@link org.springframework.security.authentication.dao.DaoAuthenticationProvider}
 * to perform login authentication.
 *
 * <p>For more sophisticated implementation, see:</p>
 *
 * @see {@link org.springframework.security.provisioning.JdbcUserDetailsManager}
 * @see {@link org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl}
 */
@Component
class AuthUserManager implements UserDetailsService {
  private static final Logger logger = LoggerFactory.getLogger(AuthUserManager.class)
  private static final String ROLE_PREFIX = "ROLE_"

  private final AuthUserDAO authUserDAO

  @Autowired
  AuthUserManager(AuthUserDAO authUserDAO) {
    this.authUserDAO = authUserDAO
  }

  @Override
  UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    logger.debug "Looking for user details for {}", username
    final user = authUserDAO.find(username)
    logger.debug "found! ${user.id} - ${user.username} (${user.roles})"
    userDetails user
  }

  void createUser(String username, String password, Set<Role> authorities) {
    try {
      authUserDAO.create(username, password,
          authorities.stream().map{it.toString()}.collect(Collectors.joining(",")))
    } catch (DuplicateKeyException e) {
      throw new IllegalArgumentException("user $username already exist")
    }
  }

  private static UserDetails userDetails(AuthUser user) {
    new User(user.username, user.hashedPassword, user.enabled,
        true, true, true,
        parsedAuthorities(user.roles))
  }

  private static List<GrantedAuthority> parsedAuthorities(String[] roles) {
    Arrays.stream(roles).map{new SimpleGrantedAuthority(ROLE_PREFIX + it)}.collect(Collectors.toList())
  }
}
