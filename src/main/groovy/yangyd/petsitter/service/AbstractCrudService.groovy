package yangyd.petsitter.service

import org.springframework.data.repository.CrudRepository

abstract class AbstractCrudService<T> {
  abstract protected CrudRepository<T, Long> getRepository()

  T findById(Long id) {
    repository.findOne(id)
  }

  Iterable<T> findAll() {
    repository.findAll()
  }

  T save(T entity) {
    repository.save(entity)
  }

  void delete(Long id) {
    repository.delete(id)
  }

  void delete(Iterable<T> entities) {
    repository.delete(entities)
  }
}
