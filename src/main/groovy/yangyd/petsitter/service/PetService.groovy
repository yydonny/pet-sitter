package yangyd.petsitter.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import yangyd.petsitter.entity.Pet
import yangyd.petsitter.entity.User
import yangyd.petsitter.repository.PetRepository

@Service
class PetService extends AbstractCrudService<Pet> {
  final PetRepository repository

  @Autowired
  PetService(PetRepository repository) {
    this.repository = repository
  }

  Pet findByOwner(User owner, String name) {
    repository.findByOwner(owner, name)
  }

  Iterable<Pet> findByOwner(User owner) {
    repository.findByOwner(owner)
  }
}
