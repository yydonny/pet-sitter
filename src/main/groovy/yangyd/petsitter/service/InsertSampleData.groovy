package yangyd.petsitter.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.transaction.PlatformTransactionManager
import yangyd.petsitter.entity.Pet
import yangyd.petsitter.entity.User
import yangyd.petsitter.entity.enums.PetType
import yangyd.petsitter.entity.enums.UserType
import yangyd.petsitter.model.Role
import yangyd.petsitter.repository.PetRepository
import yangyd.petsitter.repository.UserRepository
import yangyd.petsitter.service.auth.AuthUserManager

import javax.annotation.PostConstruct

@Component
@Profile(["development", "test"])
class InsertSampleData {

  private static final String CREATE_USER_SQL =
      "insert into auth.users" +
          " (username, password, enabled, usertype, created_at, version)" +
          " values (?,?,?,?,?,?)"

  private final PlatformTransactionManager transactionManager
  private final UserRepository userRepository
  private final PetRepository petRepository
  private final AuthUserManager userManager

  @Autowired
  InsertSampleData(PlatformTransactionManager transactionManager, UserRepository userRepository, PetRepository petRepository, AuthUserManager userManager) {
    this.transactionManager = transactionManager
    this.userRepository = userRepository
    this.petRepository = petRepository
    this.userManager = userManager
  }

  @PostConstruct
  void init() {
    Set<Role> a = new HashSet<>()
    a.addAll(Role.USER, Role.ADMIN)
    try {
      userManager.createUser("arya", "asdf1234", a)
      userManager.createUser("rob", "asdf1234", a)
    } catch (IllegalArgumentException e) {
      // so be it
    }

//    userRepository.deleteAll()
//
//    int[] uids = new TransactionTemplate(transactionManager).execute {
//      final uidArya = createUser("arya", 'asdf1234', UserType.owner)
//      final uidRob = createUser('rob', 'asdf1234', UserType.owner)
//      return [uidArya, uidRob]
//    }
//
//    // child navigation only works in session. (spring data methods close session on return)
//    new TransactionTemplate(transactionManager).execute {
//      final arya = userRepository.findOne(uids[0])
//      final rob = userRepository.findOne(uids[1])
//
//      def p1 = cat(arya, "cat1")
//      arya.pets.add(p1)
//      userRepository.save(arya) // no cascading save (not configured)
//
//      def p2 = dog(rob, "dog1")
//      userRepository.save(rob)
//      petRepository.save(p2)
//
//      System.err.println(userRepository.countUsers())
//      System.err.println(userRepository.findOneByUsername("user1").pets)
//      System.err.println(userRepository.findOneByUsername("user2").pets)
//    }
  }

  private long createUser(String username, String password, UserType userType) {

  }

  static Pet cat(User user, String name) {
    Pet p = new Pet()
    p.owner = user
    p.name = name
    p.createdAt = new Date()
    p.petType = PetType.cat
    p
  }

  static Pet dog(User user, String name) {
    Pet p = new Pet()
    p.owner = user
    p.name = name
    p.petType = PetType.dog
    p
  }
}
