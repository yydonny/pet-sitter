package yangyd.petsitter.entity

/**
 * The SQL schema name under which the entities reside.
 * (!!!) Must be created manually:
 *   {@code create schema petsitter;}
 */
interface SchemaName {
  String petsitter = "petsitter"
  String auth = "auth"
}
