package yangyd.petsitter.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.format.annotation.DateTimeFormat

import javax.persistence.*
import javax.validation.constraints.NotNull

@MappedSuperclass
abstract class AbstractEntity implements Serializable {
  @JsonIgnore
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(updatable = false)
  Long id

  @JsonIgnore
  @Version
  int version

  @JsonIgnore
  @Column(name = "created_at", nullable = false)
  @NotNull
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  Date createdAt = new Date()

  boolean equals(o) {
    if (this.is(o)) return true
    if (getClass() != o.class) return false

    AbstractEntity that = (AbstractEntity) o

    if (id != that.id) return false

    return true
  }

  int hashCode() {
    return (id != null ? id.hashCode() : 0)
  }
}
