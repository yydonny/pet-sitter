package yangyd.petsitter.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.validator.constraints.NotEmpty
import yangyd.petsitter.entity.enums.UserType

import javax.persistence.*
import javax.validation.constraints.NotNull
import java.security.Principal

@NamedQueries([
  @NamedQuery(name=User.by_username, query = User.by_username_q),
  @NamedQuery(name=User.by_username_exact, query = User.by_username_exact_q)
])
@Entity
@Table(schema = SchemaName.petsitter, name = "user_detail")
//@SequenceGenerator(name = "user_seq", allocationSize = 1) // using postgres bigserial type
class User implements Principal {
  static final String by_username = "by_username"
  private static final String by_username_q = "from User u where username like :username"

  static final String by_username_exact = "by_username_exact"
  private static final String by_username_exact_q = "from User u where username = :username"

  // user ID is to be consistent with auth.users
  // so we don't let Hibernate to generate it
  @JsonIgnore
  @Id
  @Column(updatable = false)
  Long id

  @JsonIgnore
  @Version
  int version

  @NotEmpty
  @Column(nullable = false, unique = true)
  String username

  @NotEmpty
  @Column(nullable = false, unique = false)
  String password

  @Column(nullable = false, unique = false)
  boolean enabled

  @Column(name = "first_name")
  String firstName

  @Column(name = "last_name")
  String lastName

  @NotNull
  @Enumerated(EnumType.ORDINAL)
  UserType userType

  @JsonIgnore
  @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE)
  final Set<Pet> pets = new HashSet<>()

  @Override
  String getName() {
    username
  }

  @Override
  String toString() {
    "User{$username ($id)}"
  }
}
