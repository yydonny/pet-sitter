package yangyd.petsitter.entity

import org.hibernate.validator.constraints.NotEmpty
import yangyd.petsitter.entity.enums.PetType

import javax.persistence.*

@Entity
@Table(schema = SchemaName.petsitter)
class Pet extends AbstractEntity {

  @NotEmpty
  @Column(nullable = false, unique = true)
  String name

  Integer age
  String rfid

  @ManyToOne
  @JoinColumn(name = "owner_id", nullable = false)
  User owner

  @Enumerated(EnumType.ORDINAL)
  PetType petType


  @Override
  public String toString() {
    return "Pet{" +
        "name='" + name + '\'' +
        '}';
  }
}
