package yangyd.petsitter.repository.jdbc

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementCreator
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.jdbc.support.KeyHolder
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import yangyd.petsitter.model.AuthUser

import javax.annotation.PostConstruct
import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement

@Component
class AuthUserDAO {
  public static final String USERS_BY_USERNAME_QUERY = $/
   select id,username,password,enabled,roles 
   from auth.users 
   where username = ?
  /$.trim()
  public static final String CREATE_USER_SQL = $/
    insert into auth.users (username, password, roles, enabled) 
    values (?,?,?,?)
  /$.trim()

  private static final AuthUserMapper authUserMapper = new AuthUserMapper()

  private final JdbcTemplate jdbcTemplate
  private final PasswordEncoder passwordEncoder
//  private final PlatformTransactionManager transactionManager
//  private DataSource dataSource

  @Autowired
  AuthUserDAO(PasswordEncoder passwordEncoder,
              DataSource dataSource) {
    this.passwordEncoder = passwordEncoder
    jdbcTemplate = new JdbcTemplate(dataSource)
  }

  @PostConstruct
  void init() {
    if (!(passwordEncoder instanceof BCryptPasswordEncoder)) {
      throw new IllegalStateException("Password encrypter is not correctly configured")
    }
  }

  /**
   * @param username
   * @return the {@link AuthUser} object
   * @throws UsernameNotFoundException if user not found
   */
  public AuthUser find(String username) throws UsernameNotFoundException {
    List<AuthUser> results = jdbcTemplate.query({ conn ->
      final statement = conn.prepareStatement(USERS_BY_USERNAME_QUERY)
      statement.setString(1, username)
      statement
    } as PreparedStatementCreator, authUserMapper)
    if (results.size() != 1) {
      throw new UsernameNotFoundException("can't find user $username")
    }
    results.head()
  }

  /**
   * @param username
   * @param password
   * @param roles Comma separated list of roles
   * @return
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public long create(String username, String password, String roles) {
    KeyHolder keyHolder = new GeneratedKeyHolder()
    jdbcTemplate.update({ conn ->
      final statement = conn.prepareStatement(CREATE_USER_SQL, Statement.RETURN_GENERATED_KEYS)
      statement.setString(1, username)
      statement.setString(2, passwordEncoder.encode(password))
      statement.setString(3, roles)
      statement.setBoolean(4, true)
      statement
    } as PreparedStatementCreator, keyHolder)
    (long) keyHolder.getKeys().get('id')
  }

  public void delete(String username) {
  }

  private static class AuthUserMapper implements RowMapper<AuthUser> {
    @Override
    AuthUser mapRow(ResultSet rs, int rowNum) throws SQLException {
      final AuthUser user = new AuthUser()
      user.id = rs.getLong(1)
      user.username = rs.getString(2)
      user.hashedPassword = rs.getString(3)
      user.enabled = rs.getBoolean(4)
      user.roles = rs.getString(5).split(",")
      user
    }
  }
}
