package yangyd.petsitter.repository.jdbc

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.AfterThrowing
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Component
import yangyd.petsitter.service.UserService

@Aspect
@Component
class PetJdbcRepoMonitor {
  private static final Logger logger = LoggerFactory.getLogger(PetJdbcRepoMonitor.class)

  @Pointcut("execution(public * yangyd.petsitter.repository.jdbc.*.findOne(..))")
  def pointcut1() {
  }

  @Pointcut("@annotation(org.springframework.security.access.annotation.Secured)")
  def securedMethod() {
  }

  @Pointcut("execution(* yangyd.petsitter.*.UserService+.update**(..)) && args(id, name) && target(userService)")
  def complexPointcut(UserService userService, Long id, String name) {
    // capture method acceptor and parameters
  }

  @Before("pointcut1() && securedMethod()")
  def beforeFindOne(JoinPoint joinPoint) {
    logger.info(" --------------> entering {}", joinPoint.signature.name)
  }

  @Around(
      // use a parameter as simple way to declare annotation criteria.
      "execution(* *(..)) && @annotation(secured)"
  )
  def securedBenchmark(ProceedingJoinPoint joinPoint, Secured secured) {
    logger.info("---------------- method annotated as: @Secured({})", secured.value())
    joinPoint.proceed()
  }

  @Before("complexPointcut(userService, id, name)")
  def workWithComplexPointCut(UserService userService, Long id, String name) {
    // no need for jointPoint
  }

  @AfterReturning(value = "complexPointcut(userService, id, name)", returning = "result")
  def wrapper(UserService userService, Long id, String name, int result) {
    // capture return value
  }

  @AfterThrowing(value = "complexPointcut(userService, id, name)", throwing = "e")
  def someUniversalExceptionHandling(UserService userService, Long id, String name, Exception e) {
  }

}
