package yangyd.petsitter.repository.jdbc

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import yangyd.petsitter.entity.Pet

@Repository
@Profile("test")
class PetJdbcRepo {
  public static final String find_by_id_sql = "" +
      "select id, name " +
      "from petsitter.pet " +
      "where id = ?"

  private final JdbcTemplate jdbcTemplate

  @Autowired
  PetJdbcRepo(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate
  }

  @Transactional(
      transactionManager = "dsTxManager",// in case there're more than one txManager
      propagation = Propagation.REQUIRED,
      readOnly = true)
  @Secured("findOne")
  Optional<Pet> findOne(long id) {
    try {
      Pet p = jdbcTemplate.queryForObject(find_by_id_sql, { rs, row ->
        newPet(rs.getLong("id"), rs.getString("name"))
      } as RowMapper<Pet>, id)
      Optional.of(p)
    } catch (IncorrectResultSizeDataAccessException ignored) {
      Optional.empty()
    }
  }

  private static def newPet(long id, String name) {
    def p = new Pet()
    p.id = id
    p.name = name
    p
  }
}
