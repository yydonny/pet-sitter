package yangyd.petsitter.repository.jdbc

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import yangyd.petsitter.entity.User

import java.sql.ResultSet

@Repository
@Profile("test")
class UserJdbcRepo {
  private final JdbcTemplate jdbcTemplate

  @Autowired
  UserJdbcRepo(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate
  }

  List<Map<String, Object>> listUsers() {
    jdbcTemplate.queryForList("select * from p_user")
  }

  @Transactional(transactionManager = "dsTxManager",
      propagation = Propagation.REQUIRED,
      rollbackFor = NullPointerException.class, // rollback on this error
      noRollbackFor = IllegalArgumentException.class // but not this
  )
  int createUser(Long id, String name, String passwd, String email) {
    jdbcTemplate.update("insert into p_user(id, username, password, email) values(?,?,?,?)",
        id,name,passwd,email)
  }

  long countUsers() {
    jdbcTemplate.queryForObject("select count(*) from p_user", Long)
  }

  @Transactional(
      transactionManager = "dsTxManager",
      propagation = Propagation.NESTED, readOnly = true)
  List<User> findByName(String name) {
    def find_by_name_sql = "select id, username from p_user where username like '%'||?||'%'"
    jdbcTemplate.query(find_by_name_sql, { rs, row -> user(rs) } as RowMapper<User>, name)
  }

  private static User user(ResultSet rs) {
    User u = new User()
    u.id = rs.getLong("id")
    u.username = rs.getString("username")
    u
  }
}
