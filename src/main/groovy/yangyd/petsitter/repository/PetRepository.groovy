package yangyd.petsitter.repository

import org.springframework.data.repository.CrudRepository
import yangyd.petsitter.entity.Pet
import yangyd.petsitter.entity.User

interface PetRepository extends CrudRepository<Pet, Long> {
  Pet findByOwner(User owner, String name)
  Iterable<Pet> findByOwner(User owner)
}
