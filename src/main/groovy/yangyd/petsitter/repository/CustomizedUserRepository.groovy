package yangyd.petsitter.repository

import org.springframework.stereotype.Repository
import yangyd.petsitter.entity.User

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class CustomizedUserRepository {

  // application servers usually use setter injection
  @PersistenceContext
  EntityManager entityManager

  List<User> findAllByUsername(String username, boolean exact) {
    if (exact) {
      entityManager.createNamedQuery(User.by_username_exact)
          .setParameter("username", username).resultList
    } else {
      entityManager.createNamedQuery(User.by_username)
          .setParameter("username", "%$username%".toString()).resultList // for Object parameter, GString will be passed instead of String
    }
  }

  List<User> findAllByUsername2(String lastname) {
    def builder = entityManager.criteriaBuilder
    def param1 = builder.parameter(String)

    def query = builder.createQuery(User)
    def userRoot = query.from(User)
    def criteria = builder.equal(userRoot.get("username"), param1)
    query.select(userRoot).where(criteria)

    def q = entityManager.createQuery(query)
    q.setParameter(param1, lastname)
    q.resultList
  }
}
