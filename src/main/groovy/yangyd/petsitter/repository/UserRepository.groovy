package yangyd.petsitter.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional
import yangyd.petsitter.entity.User

@Transactional
interface UserRepository extends CrudRepository<User, Long> {
  @Query("select u from User u where u.username like %?1%")
  List<User> findAllByUsername(String username)

  @Query("select u from User u where u.username = :un")
  User findOneByUsername(@Param("un") String username)

  @Query("select u.username from User u where u.id = :id")
  String findUsername(Long id)

  @Query("select count(u) from User u")
  long countUsers()
}
