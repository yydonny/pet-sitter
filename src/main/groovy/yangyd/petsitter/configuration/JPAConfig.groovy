package yangyd.petsitter.configuration

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import groovy.transform.CompileStatic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import yangyd.petsitter.entity.User

import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@CompileStatic
@Configuration
@EnableJpaRepositories(basePackages = "yangyd.petsitter.repository")
class JPAConfig {
  public static final String HIKARI_PROP_FILE = "/hikari.properties" // in classpath
  private static final String ENTITY_PACKAGE = User.class.package.name

  @Profile(["development","production"])
  @Bean(destroyMethod = "close")
  DataSource hikariDataSource() {
    new HikariDataSource(new HikariConfig(HIKARI_PROP_FILE))
  }

  @Bean
  EntityManagerFactory entityManagerFactory(DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean()
    factoryBean.dataSource = dataSource

    // delegate configuration to org.hibernate.cfg.Configuration (see hibernate.properties)
    factoryBean.jpaVendorAdapter = new HibernateJpaVendorAdapter()

    // or you can pass in properties directly
//    factoryBean.jpaProperties = ?

    // for java varargs, if not assigned explicitly as an Array of String,
    // (!!!) Groovy will break string into array of single characters
    factoryBean.packagesToScan = [ENTITY_PACKAGE].toArray(new String[1])

    factoryBean.afterPropertiesSet()
    factoryBean.getObject()
  }

  @Bean
  PlatformTransactionManager jpaTxManager(EntityManagerFactory entityManagerFactory) {
    new JpaTransactionManager(entityManagerFactory)
  }

  @Bean
  @Profile("test")
  PlatformTransactionManager dsTxManager(DataSource dataSource) {
    new DataSourceTransactionManager(dataSource)
  }
}
