package yangyd.spring.ch2

import org.springframework.context.support.ClassPathXmlApplicationContext
import yangyd.petsitter.entity.User
import yangyd.spring.ch2.model.Person

class Ch2Main {
  static def startup() {
    def context = new ClassPathXmlApplicationContext("classpath:ch2/application.xml")
    def userService = context.getBean(SimpleUserService)
    def bar = context.getBean("bar2")
    def person = context.getBean(Person)
    def someBean = context.getBean("someBean")
    println(bar)
    println(person)
    println(someBean.class.simpleName)
    userService.save(new User())

    for (name in context.beanDefinitionNames) {
      def bean = context.getBean(name)
      System.err.println("$name -> ${bean.getClass().simpleName}")
    }

    context.close() // call all finalization method
  }

}
