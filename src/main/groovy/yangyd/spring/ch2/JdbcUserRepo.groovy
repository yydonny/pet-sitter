package yangyd.spring.ch2

import javax.sql.DataSource

class JdbcUserRepo {
    private DataSource dataSource

    DataSource getDataSource() {
        return dataSource
    }

    void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource
    }
}
