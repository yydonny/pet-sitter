package yangyd.spring.ch2

import org.springframework.beans.PropertyEditorRegistrar
import org.springframework.beans.PropertyEditorRegistry
import org.springframework.beans.propertyeditors.CustomDateEditor

import java.text.SimpleDateFormat

class DateConverter implements PropertyEditorRegistrar {
  @Override
  void registerCustomEditors(PropertyEditorRegistry registry) {
    registry.registerCustomEditor(Date, new CustomDateEditor(
        new SimpleDateFormat("yyyy-MM-dd"), false))



  }
}
