package yangyd.spring.ch2

import org.springframework.beans.factory.FactoryBean
import yangyd.spring.ch2.model.SomeBean

class SomeBeanFactory implements FactoryBean<SomeBean> {
  @Override
  SomeBean getObject() throws Exception {
    new SomeBean()
  }

  @Override
  Class<?> getObjectType() { SomeBean }

  @Override
  boolean isSingleton() { false }
}
