package yangyd.spring.ch2.model

import javax.annotation.PreDestroy

class FooBean {

  // public make it a plain Java static, instead of bean property
  public static final FooBean THIS_FOO = new FooBean(new Date())
  final Date date

  def k = { n -> n + n }

  FooBean(Date date) {
    this.date = date
  }

  void close1() {
    println("----------------------------- Done close1 ------------------------------------")
  }

  @PreDestroy
  void close2() {
    println("----------------------------- Done PreDestroy --------------------------------")
  }

  @Override
  String toString() {
    "i'm foo of $date, ${k 12}"
  }
}
