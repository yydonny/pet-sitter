package yangyd.spring.ch2.model

import javax.annotation.PostConstruct

class BarBean {
  final FooBean foo
  BarBean(FooBean foo) {
    this.foo = foo
  }

  void init1() {
    println("-------------------- BarBean initialization --------------------------")
  }

  @PostConstruct
  void init2() {
    println("-------------------- JSR PostConstruct --------------------------")
  }
  @Override
  String toString() {
    "I'm bar, and here's foo: $foo"
  }
}
