package yangyd.spring.ch2.model

class Person {
  static final Person

  String fullName
  Date birthDay

  Person() {
  }

  Person(String fullName, Date birthDay) {
    this.fullName = fullName
    this.birthDay = birthDay
  }

  @Override
  String toString() {
    "Person{" + "fullName='" + fullName + '\'' + ", birthDay=" + birthDay + '}'
  }
}
