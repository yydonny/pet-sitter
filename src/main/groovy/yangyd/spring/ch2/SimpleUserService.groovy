package yangyd.spring.ch2

import yangyd.petsitter.entity.User

class SimpleUserService {
    private JdbcUserRepo repo

    JdbcUserRepo getRepo() {
        return repo
    }

    void setRepo(JdbcUserRepo repo) {
        this.repo = repo
    }

    def save(User user) {
        println("save user " + user)
    }
}
