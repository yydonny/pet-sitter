package yangyd.petsitter.service

import yangyd.petsitter.configuration.JPAConfig
import yangyd.petsitter.entity.Pet
import yangyd.petsitter.entity.User

import static yangyd.petsitter.service.InsertSampleData.*

class Fixture {
  // add public because we don't want constant to become properties
  public static final User marukoChan = owner("maruko-chan")
  public static final Pet kuro = cat(marukoChan, "kuro")
  public static final Pet shiro = dog(marukoChan, "shiro")

  static void initSystemProps() {
    System.setProperty(JPAConfig.HIKARI_PROP_FILE, "/hikari.properties")
  }

}
