package yangyd.petsitter.service

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import yangyd.petsitter.repository.PetRepository

import static org.mockito.Mockito.when
import static yangyd.petsitter.service.Fixture.*

@RunWith(MockitoJUnitRunner)
class PetServiceTest {
  @Mock
  private PetRepository mockRepository

  @InjectMocks
  private PetService petService

  @Test
  void findByOwner_test1() {
    when(mockRepository.findByOwner(marukoChan, kuro.name)).thenReturn(kuro)
    Assert.assertSame(kuro, petService.findByOwner(marukoChan, kuro.name))
  }

  @Test
  void findByOwner_test2() {
    when(mockRepository.findByOwner(marukoChan)).thenReturn([kuro, shiro].toList())
    Assert.assertEquals(2, petService.findByOwner(marukoChan).toList().size())
  }
}
