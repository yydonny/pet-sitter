package yangyd.petsitter.service

import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.rules.SpringClassRule
import org.springframework.test.context.junit4.rules.SpringMethodRule
import yangyd.petsitter.repository.CustomizedUserRepository
import yangyd.petsitter.repository.UserRepository

@RunWith(BlockJUnit4ClassRunner)
@ContextConfiguration(locations = [
    "classpath:/application.xml",
    "classpath:/test-db.xml"
])
@ActiveProfiles(["test"]) // wire profile-specific bean
class UserRepoIntegrationTest {
  @ClassRule
  public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule()
  @Rule
  public final SpringMethodRule springMethodRule = new SpringMethodRule()

  @BeforeClass
  static void init() {
    Fixture.initSystemProps()
  }

  @Autowired
  private CustomizedUserRepository customizedUserRepo

  @Autowired
  private UserRepository userRepo

  @Test
  void test1() {
    userRepo.save(Fixture.marukoChan)
    customizedUserRepo.findAllByUsername("maruko", false).forEach({System.err.println(it)})
    customizedUserRepo.findAllByUsername2("maruko-chan").forEach({System.err.println(it)})
  }

}
