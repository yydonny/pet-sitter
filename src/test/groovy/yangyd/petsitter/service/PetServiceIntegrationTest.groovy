package yangyd.petsitter.service

import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.rules.SpringClassRule
import org.springframework.test.context.junit4.rules.SpringMethodRule
import yangyd.petsitter.repository.jdbc.PetJdbcRepo

import java.util.concurrent.atomic.AtomicReference

import static yangyd.petsitter.service.Fixture.kuro

@RunWith(BlockJUnit4ClassRunner)
@ContextConfiguration(locations = [
    "classpath:/application.xml",
    "classpath:/test-db.xml"
])
@ActiveProfiles(["test"]) // wire profile-specific bean
class PetServiceIntegrationTest {
  // use spring junit rules so that the suite doesn't need to @RunWith spring runner
  @ClassRule
  public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule()
  @Rule
  public final SpringMethodRule springMethodRule = new SpringMethodRule()

  @BeforeClass
  static void init() {
    Fixture.initSystemProps()
  }

  @Autowired
  private PetService petService

  @Autowired
  private UserService userService

  @Autowired
  private PetJdbcRepo petJdbcRepo

  private static AtomicReference<Long> userID = new AtomicReference<Long>()
  private static AtomicReference<Long> petID = new AtomicReference<>()

  @Test
  void test0() {
    def p = petJdbcRepo.findOne(1)
    Assert.assertFalse(p.isPresent())
  }

  @Test
  void test1() {
    userID.set(userService.save(kuro.owner).id)
    petID.set(petService.save(kuro).id)
  }

  @Test
  void test2() {
    Assert.assertNotNull(petID.get())
    Assert.assertNotNull(userID.get())
    def pet = petService.findById(petID.get())
    Assert.assertNotNull(pet)
    Assert.assertNotNull(pet.owner)
  }

  @Test
  void test3() {
    petService.delete(petID.get())
    userService.delete(userID.get())
  }

  @Test
  void test4() {
    Assert.assertNull(userService.findById(userID.get()))
    Assert.assertNull(petService.findById(petID.get()))
  }
}
