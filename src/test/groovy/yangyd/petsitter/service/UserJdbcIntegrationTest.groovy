package yangyd.petsitter.service

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlConfig
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.BeforeTransaction
import org.springframework.transaction.annotation.Transactional
import yangyd.petsitter.repository.jdbc.UserJdbcRepo

@RunWith(SpringJUnit4ClassRunner)
@ContextConfiguration(locations = [
    "classpath:/application.xml",
    "classpath:/test-db.xml"
])
@ActiveProfiles("test")
class UserJdbcIntegrationTest {
  @Autowired
  private UserJdbcRepo userJdbcRepo

  @BeforeTransaction
  void checkInit() {
    Assert.assertEquals(4, userJdbcRepo.countUsers())
  }

  @Test
  @Sql(scripts = "classpath:/data/extra-data.sql",
      config = @SqlConfig(encoding = "utf-8", separator = ";", commentPrefix = "--"))
  void test1() {
    Assert.assertEquals(5, userJdbcRepo.countUsers())
  }

  @Test
  @Sql(statements = "delete from p_user where username = 'meow'",
      config = @SqlConfig(transactionManager = "dsTxManager",
          transactionMode = SqlConfig.TransactionMode.ISOLATED),
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  void test2() {
    Assert.assertEquals(0, userJdbcRepo.findByName("meow").size())
  }

  @Transactional(transactionManager = "dsTxManager")
  @Rollback // leave db unchanged before and after this case
  @Test
  void test3() {
    int k = userJdbcRepo.createUser(5,"yangyd","asdf1234","yangyd@live.com")
    Assert.assertEquals(1, k)
    def users = userJdbcRepo.listUsers()
    users.forEach({
      println(it)
    })
    Assert.assertFalse(userJdbcRepo.findByName("yangyd").empty)
  }

  @Test
  void test4() {
    Assert.assertEquals(4, userJdbcRepo.countUsers())
  }
}
