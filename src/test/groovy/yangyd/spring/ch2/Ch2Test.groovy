package yangyd.spring.ch2

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.springframework.context.support.ClassPathXmlApplicationContext

@RunWith(BlockJUnit4ClassRunner)
class Ch2Test {
  @Test
  void test1() {
    def ctx = new ClassPathXmlApplicationContext("classpath:ch2/spring-beans.xml")
    Assert.assertNotNull(ctx.getBean("dataSource"))
  }
}
